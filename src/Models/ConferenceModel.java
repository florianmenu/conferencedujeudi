
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;


import Database.pojo.Conference;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Florian
 */
public class ConferenceModel extends AbstractTableModel{
    
    // declaration des attribut
    private final String[] enTete = new String[]{"Titre", "Date", "Conférencier", "Salle"};
    private ArrayList<Conference> listeDeConference = new ArrayList<>();

    //Creation du contructeur avec arrayliste en params.
    public ConferenceModel(ArrayList<Conference> _listeDeConference){
        this.listeDeConference = _listeDeConference;
    }
 
    @Override
    public int getRowCount() {
        return listeDeConference.size();
    }

    @Override
    public int getColumnCount() {
        return enTete.length;
    }
    
    @Override
    public String getColumnName(int columnIndex){
        return enTete[columnIndex];
    }

    
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conference maConf = listeDeConference.get(rowIndex);
        switch(columnIndex){
            case 0:
                return maConf.getTitreConference(); 
            
            case 1:
                return maConf.getDateString();
            
            case 2:
                return maConf.getNomConferencier();
                
            case 3:
                return maConf.getIdSalle();
            
            default: 
                return "";
            
        }
    }
}
