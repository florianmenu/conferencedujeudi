/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi;

import Panel.Home;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 *
 * @author Fabrice_a
 */
public class CustomWindow extends JFrame implements ActionListener {

    JMenuItem menuItem;

    public CustomWindow() throws SQLException {
        // defini un titre pour la fenetre
        this.setTitle("Ma première fenêtre Java");

        // defini la taille de la fenetre
        this.setSize(1024, 720);

        // positionnement de la fenetre au centre de l'écran
        this.setLocationRelativeTo(null);

        // termine la fenetre si on clique sur la croix
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // barre Menu
        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        // Menu fichier
        JMenu menuFichier = new JMenu("Fichier");
        menuBar.add(menuFichier);
        // Menu fichier > Fermer
        menuItem = new JMenuItem("Fermer");
        menuFichier.add(menuItem);
        menuItem.addActionListener(this);
        // affectation de l'accélérateur ctrl+z
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK, true));

        // Menu Conférence
        JMenu menuConference = new JMenu("Conférence");
        menuBar.add(menuConference);
        // Menu Conférence > Voir la liste des conférences
        JMenuItem menuItemVoirListe = new JMenuItem("Voir la liste des conférences");
        menuConference.add(menuItemVoirListe);
        // Menu Conférence > Ajouter une conférence
        JMenuItem menuItemAjoutConf = new JMenuItem("Ajouter une conférence");
        menuConference.add(menuItemAjoutConf);

        // Panel
        // Création d'une instance de Home afin de l'afficher dans notre Fenêtre
        Home panel = new Home();
        this.setContentPane(panel);



        // Rafraichie la fenêtre 
        repaint();
        revalidate();

        // rend la fenetre visible
        this.setVisible(true);
    }

    // Sur le clic du sous menu "Fermer" fermeture du programme.
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuItem) {
            System.exit(0);
        }
    }

}
