/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author Florian
 */
public class DatabaseUtilities {
    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch (ClassNotFoundException e){
                    e.printStackTrace();
                    return null;
        }
        Connection connection = null;
        try {
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/conferencedujeudi","root","");
            
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return connection;
    }
    public static ResultSet exec(String _query) throws SQLException{
        //ouverture de la connection
        try{
        Connection _c = getConnexion();
        Statement stmt = null;
        stmt = _c.createStatement(); 
        String query2 = _query;
        ResultSet resultSet = stmt.executeQuery(query2);
        return resultSet;

      } catch (Exception ex) {
            System.err.println(ex);
      }
        return null;
    }

    
}
