/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.pojo;

/**
 *
 * @author Florian
 */
public class Salarier {
    private int id_salarie;
    private String nom_prenom;
    
    public Salarier(int id_salarie, String nom_prenom){
        this.id_salarie = id_salarie;
        this.nom_prenom = nom_prenom;
    }
    
    public int getIdSalarie(){
        return this.id_salarie;
    }
    
    public void setIdSalarie(int id_salarie){
        this.id_salarie = id_salarie;
    }
    
    public String getNomPrenomSalarie(){
        return this.nom_prenom;
    }
    
    public void setNomPrenomSalarie(String nom_prenom){
        this.nom_prenom = nom_prenom;
    }
}
