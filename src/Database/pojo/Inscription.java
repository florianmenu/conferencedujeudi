/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.pojo;

/**
 *
 * @author Florian
 */
public class Inscription {
    private int idInscription;
    private int idSalarie;
    private int idConference;
    
    public Inscription(int _idInscription, int _idSalarie, int _idConference){
        this.idInscription = _idInscription;
        this.idSalarie = _idSalarie;
        this.idConference = _idConference;
    }
    
    public int getIdInscription(){
        return this.idInscription;
    }
    
    public void setIdInscription(int _idInscription){
        this.idInscription = _idInscription;
    }

    public int getIdSalarie(){
        return this.idSalarie;
    }
    
    public void setIdSalarie(int _idSalarie){
        this.idSalarie = _idSalarie;
    }

    public int getIdConference(){
        return this.idConference;
    }
    
    public void setIdConference(int _idConference){
        this.idConference = _idConference;
    }

}
