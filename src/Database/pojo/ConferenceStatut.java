/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.pojo;

import java.util.Calendar;

/**
 *
 * @author Florian
 */
public class ConferenceStatut {
    private int is_statut;
    private int id_conf;

    
    public ConferenceStatut(int _idConference, int _idStatut){
        this.id_conf = _idConference;
        this.is_statut = _idStatut;
      
    }
    
    public int getIdConference(){
         return this.id_conf;
    }
    
    public void setIdConference(int _idConference){
        this.id_conf = _idConference;
    }

    public int getidStatut(){
        return this.is_statut;
    }
    
    public void setidStatut(int _idStatut){
        this.is_statut = _idStatut;
    }


}
