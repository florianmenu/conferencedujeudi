/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.pojo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Florian
 */
public class Conference {
    private int id_conf;
    private String titre_conf;
    private Calendar date_conf;
    private int id_conferencier;
    private int id_salle;
    private int id_theme;
    private String nom_conferencier;
    private java.sql.Date sqlDate;

    public Conference(){
    }
    
    public Conference(int _idConference, String _titreConference, int _idConferencier){
        this.id_conf = _idConference;
        this.titre_conf = _titreConference;
        this.id_conferencier = _idConferencier;
    }
    
    public Conference(String _titreConference, Calendar _dateConference,int _idConferencier, int _idSalle, int _idTheme){
        this.titre_conf = _titreConference;
        this.date_conf = _dateConference;
        this.id_conferencier = _idConferencier;
        this.id_theme = _idTheme;
        this.id_salle = _idSalle;
        
    }
    
    public Conference(int _idConference, String _titreConference, Calendar _dateConference,int _idConferencier, int _idSalle, int _idTheme){
        this.id_conf = _idConference;
        this.titre_conf = _titreConference;
        this.date_conf = _dateConference;
        this.id_conferencier = _idConferencier;
        this.nom_conferencier = getNomConferencier(_idConferencier);
        this.id_salle = _idSalle;
        this.id_theme = _idTheme;
    }
    
    public int getIdConference(){
        return this.id_conf;
    }
    
    public void setIdConference(int _idConference){
        this.id_conf = _idConference;
    }
    
    public String getTitreConference(){
        return this.titre_conf;
    }
    
    public void setTitreConference(String _titreConference){
        this.titre_conf = _titreConference;
    }
    
    public Calendar getDateConference(){
        return this.date_conf;
    }
    
    public void setDateConference(Calendar _dateCalendar){
        this.date_conf = _dateCalendar;
    }
    
    public int getIdConferencier(){
        return this.id_conferencier;
    }
    
    public void setIdConferencier( int _idConferencier){
        this.id_conferencier = _idConferencier;
    }
    
    public String getNomConferencier(){
        return this.nom_conferencier;
    }
    
    public void setNomConferencier( String _nomConferencier){
        this.nom_conferencier = _nomConferencier;
    }
    
    private String getNomConferencier(int _idConferencier) {
        return this.nom_conferencier;
    }
        
    public int getIdSalle(){
        return this.id_salle;
    }
    
    public void setIdSalle(int _idSalle){
        this.id_salle = _idSalle;
    }
    
    
    public int getIdTheme(){
        return this.id_theme;
    }
    
    public void setIdTheme( int _idTheme){
        this.id_theme = _idTheme;
    }
    
    public java.sql.Date getSqlDate(){
        sqlDate = new java.sql.Date(this.date_conf.getTimeInMillis());
        return this.sqlDate;
    }
    
    public void setSqlDate( java.sql.Date _sqlDate){
        this.sqlDate = _sqlDate;
    }
    
    public String getDateString(){
        Calendar cal = this.date_conf;
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        String formatted = format1.format(cal.getTime());
        return formatted;
    }



}
