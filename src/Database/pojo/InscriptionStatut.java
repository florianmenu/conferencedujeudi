/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.pojo;


import java.sql.Date;
import java.util.Calendar;

/**
 *
 * @author Florian
*/
public class InscriptionStatut {
    private int idInscription;
    private int idStatut;
    private Calendar dateStatutInscription;
    private Date sqlDate;
    
    
    public InscriptionStatut(int _idInscription, int _idStatut, Calendar dateStatutInscription){
        this.idInscription = _idInscription;
        this.idStatut = _idStatut;
        this.dateStatutInscription = dateStatutInscription;
    }
    
    public int getIdInscription(){
        return this.idInscription;
    }
    
    public void setIdInscription(int _idInscription){
        this.idInscription = _idInscription;
    }
    
    public int getIdStatut(){
        return this.idInscription;
    }
    
    public void setIdStatut(int _idStatut){
        this.idStatut = _idStatut;
    }

    public Calendar getDateStatutInscription(){
        return this.dateStatutInscription;
    }

    public java.sql.Date getSqlDate(){
        Date sqlDate = new java.sql.Date(this.dateStatutInscription.getTimeInMillis());
        return this.sqlDate;
    }
        
    public void setSqlDate( java.sql.Date _sqlDate){
        this.sqlDate = _sqlDate;
    }
    
    public void setDateStatutInscription(Calendar _dateStatutInscription){
        this.dateStatutInscription = _dateStatutInscription;
    }

    
}