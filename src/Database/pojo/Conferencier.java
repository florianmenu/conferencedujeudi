/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.pojo;

/**
 *
 * @author Florian
 */
public class Conferencier {
    private int id_conferencier;
    private String nom_prenom;
    boolean interne;
    int id_conf;
    String bloc_note;
    
    public Conferencier(){        
    }
    
    public Conferencier(int _idConferencier, String _nomPrenomConferencier, int _idConference){
        this.id_conferencier = _idConferencier;
        this.nom_prenom = _nomPrenomConferencier;
        this.id_conf = _idConference;
    }
    
    public Conferencier(int _idConferencier, String _nomPrenomConferencier, boolean _conferencierInterne, int _idConference, String _blocNoteConferencier){
        this.id_conferencier = _idConferencier;
        this.nom_prenom = _nomPrenomConferencier;
        this.interne = _conferencierInterne;
        this.id_conf = _idConference;
        this.bloc_note = _blocNoteConferencier;                
    }
    
    
    public int getIdConferencier(){
        return this.id_conferencier;
    }
    
    public void setIdConferencier(int _idConferencier){
         this.id_conferencier = _idConferencier;
    }
     
    public String getNomPrenomConferencier(){
        return this.nom_prenom;
    }
    
    public void setNomPrenomConferencier(String _nomPrenomConferencier){
         this.nom_prenom = _nomPrenomConferencier;
    }
    
    public boolean getConferencierInterne(){
        return this.interne;
    }
    
    public void setConferencierInterne(boolean _conferencierInterne){
         this.interne = _conferencierInterne;
    }

    public int getIdConference(){
        return this.id_conf;
    }
    
    public void setIdConference(int _idConference){
         this.id_conf = _idConference;
    }

    public String getBlocNoteConferencier(){
        return this.bloc_note;
    }
    
    public void setBlocNoteConferencier(String _blocNoteConferencier){
         this.bloc_note = _blocNoteConferencier;
    }
    
}


