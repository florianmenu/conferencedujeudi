/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.Salle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Florian
 */
public class SalleDAO {
           //Recupération de toute les conferences
        public static ArrayList<Salle> getAll(){
            ArrayList<Salle> listeDeSalle = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT id_Salle, nom_Salle, nombre_place FROM salle";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int id_Salle = rs.getInt("id_Salle");
                    String nom_Salle = rs.getString("nom_Salle");
                    int nombre_place = rs.getInt("nombre_place");
                    Salle salle = new Salle(id_Salle, nom_Salle, nombre_place );
                    listeDeSalle.add(salle);
                }
                return listeDeSalle;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static Salle get(int id_Salle){
            PreparedStatement ps =null;
            String query = "SELECT * FROM conference WHERE id_Salle = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Salle);
                ResultSet rs = ps.executeQuery();
                Salle conf = new Salle(rs.getInt("id_Salle"), rs.getString("nom_Salle"), rs.getInt("nombre_place"));
                return conf;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(Salle c){
            PreparedStatement ps =null;
            String query = "INSERT INTO conference"
                    + "(id_Salle, nom_Salle, nombre_place)"
                    + "VALUES (?,?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, c.getIdSalle());
                ps.setString(2, c.getNomSalle());
                ps.setInt(3, c.getNbPlaceSalle());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static Salle delet(int id_Salle){
            PreparedStatement ps =null;
            String query = "DELET FROM salle WHERE id_Salle = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Salle);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(Salle salle){
            PreparedStatement ps =null;
            String query = "UPDATE salle SET "
                    + "nom_Salle = ?,"
                    + "nombre_place = ?,"
                    + "WHERE id_Salle = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);   
                ps.setString(1,salle.getNomSalle());
                ps.setInt(2, salle.getNbPlaceSalle());
                ps.setInt(3, salle.getIdSalle());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
