/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.InscriptionStatut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Florian
 */
public class InscriptionStatutDAO {
        public static ArrayList<InscriptionStatut> getAll(){
            ArrayList<InscriptionStatut> listeInscriptionStatut = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT * FROM inscription_statut";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    Calendar dateCalendar = Calendar.getInstance();
                    java.sql.Date date = rs.getDate("date");
                    dateCalendar.setTimeInMillis(date.getTime());
                    int idStatut = rs.getInt("id_statut");
                    int idInscription = rs.getInt("id_inscription");
                    
                    InscriptionStatut inscriptionStatut = new InscriptionStatut(idInscription, idStatut, dateCalendar);
                    listeInscriptionStatut.add(inscriptionStatut);
                }
                return listeInscriptionStatut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static InscriptionStatut get(int _idInscription){
            PreparedStatement ps =null;
            String query = "SELECT * FROM inscription_statut WHERE id_inscription = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idInscription);
                ResultSet rs = ps.executeQuery();
                int idInscription = rs.getInt("id_inscription");
                int idStatut = rs.getInt("id_statut");
                Calendar dateCalendar = Calendar.getInstance();
                java.sql.Date date = rs.getDate("date");
                dateCalendar.setTimeInMillis(date.getTime());
                InscriptionStatut inscriptionStatut = new InscriptionStatut(idInscription, idStatut, dateCalendar);
                return inscriptionStatut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(InscriptionStatut inscriptionStatut){
            PreparedStatement ps =null;
            String query = "INSERT INTO inscription_statut"
                    + "()"
                    + "VALUES (?,?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, inscriptionStatut.getIdStatut());
                ps.setInt(2, inscriptionStatut.getIdInscription());
                ps.setDate(3, inscriptionStatut.getSqlDate());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static InscriptionStatut delet(int _idInscription){
            PreparedStatement ps =null;
            String query = "DELET FROM inscription_statut WHERE id_inscription = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idInscription);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(InscriptionStatut Inscription){
            PreparedStatement ps =null;
            String query = "UPDATE inscription_statut SET "
                    + "id_statut = ?,"
                    + "date = ?,"
                    + "WHERE id_inscription = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);               
                ps.setInt(1, Inscription.getIdStatut());
                ps.setDate(2, Inscription.getSqlDate());
                ps.setInt(3, Inscription.getIdInscription());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
