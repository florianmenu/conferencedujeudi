/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.Conferencier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Florian
 */
public class ConferencierDAO {
        //Recupération de tous les conferenciers.
        public static ArrayList<Conferencier> getAll(){
            ArrayList<Conferencier> listeDeConferencier = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT * FROM conferencier";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int idConferencier = rs.getInt("id_conferencier");
                    String nomPrenomConferencier = rs.getString("nomPrenomConferencier");
                    Boolean indInt = rs.getBoolean("interne");
                    int idConference = rs.getInt("id_conference");
                    String indBlocNoteConferencier = rs.getString("bloc_note");
                    Conferencier conferencier = new Conferencier(idConferencier, nomPrenomConferencier, indInt , idConference, indBlocNoteConferencier);
                    listeDeConferencier.add(conferencier);
                }
                return listeDeConferencier;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'un conferencier par id.
        public static Conferencier get(int _idConferencier){
            PreparedStatement ps =null;
            String query = "SELECT * FROM conference WHERE id_conferencier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idConferencier);
                ResultSet rs = ps.executeQuery();
                int idConferencier = rs.getInt("id_conferencier");
                String nomPrenomConferencier = rs.getString("nom_prenom");
                Boolean indInt = rs.getBoolean("interne");
                int idConference = rs.getInt("id_conference");
                String indBlocNoteConferencier = rs.getString("bloc_note");
                Conferencier conferencier = new Conferencier(idConferencier, nomPrenomConferencier, indInt , idConference, indBlocNoteConferencier);
                return conferencier;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'un conferencier  
        public static String insert(Conferencier c){
            PreparedStatement ps =null;
            String query = "INSERT INTO conferencier"
                    + "(nom_prenom, interne, id_conference, bloc_note)"
                    + "VALUES (?,?,?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setString(1, c.getNomPrenomConferencier());
                ps.setBoolean(2, c.getConferencierInterne());
                ps.setInt(3, c.getIdConference());
                ps.setString(4, c.getBlocNoteConferencier());
                ps.execute(); 
                String msg="Nouveau conferencier crée";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        
        //supression d'un conferencier par id.
        public static Conferencier delet(int _idConferencier){
            PreparedStatement ps =null;
            String query = "DELET FROM conferencier WHERE id_conferencier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idConferencier);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'un conferencier par id.
        public static String update(Conferencier _conferencier){
            PreparedStatement ps =null;
            String query = "UPDATE conference SET "
                    + "nom_prenom = ?,"
                    + "interne = ?,"
                    + "id_conference = ?,"
                    + "bloc_note = ?"
                    + "WHERE id_conferencier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);               
                ps.setString(1, _conferencier.getNomPrenomConferencier());
                ps.setBoolean(2, _conferencier.getConferencierInterne());
                ps.setInt(3, _conferencier.getIdConference());
                ps.setString(4, _conferencier.getBlocNoteConferencier());
                ps.setInt(5, _conferencier.getIdConferencier());
                ResultSet rs = ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
