/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import Database.DatabaseUtilities;
import static Database.DatabaseUtilities.getConnexion;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import Database.pojo.Conference;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import conferencedujeudi.ConferenceDuJeudi;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.UIManager.getInt;
import static javax.swing.UIManager.getString;


/**
 *
 * @author Florian
 */
public class ConferenceDAO {
    

       //Recupération de toute les conferences
        public static ArrayList<Conference> getAll(){
            ArrayList<Conference> listeDeConference = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT id_conf, titre_conf, id_conferencier, id_salle , dateCalendar, id_theme FROM conference";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int id = rs.getInt("id_conf");
                    String titre = rs.getString("titre_conf");
                    int indConf = rs.getInt("id_conferencier");
                    int indTheme = rs.getInt("id_theme");
                    int indSalle = rs.getInt("id_salle");
                    Calendar dateCalendar = Calendar.getInstance();
                    java.sql.Date date = rs.getDate("dateCalendar");
                    dateCalendar.setTimeInMillis(date.getTime());
                    Conference conf = new Conference(id, titre, dateCalendar , indConf, indTheme, indSalle);
                    listeDeConference.add(conf);
                }
                return listeDeConference;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static Conference get(int _idConference){
            PreparedStatement ps =null;
            String query = "SELECT * FROM conference WHERE id_conf = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idConference);
                ResultSet rs = ps.executeQuery();
                Calendar dateCalendar = Calendar.getInstance();
                java.sql.Date date = rs.getDate("dateCalendar");
                dateCalendar.setTimeInMillis(date.getTime());
                Conference conf = new Conference(rs.getInt("id_conf"), rs.getString("titre_conf"), dateCalendar , rs.getInt("id_conferencier"), rs.getInt("id_theme"), rs.getInt("id_salle"));
                return conf;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(Conference c){
            PreparedStatement ps =null;
            String query = "INSERT INTO conference"
                    + "(titre_conf, dateCalendar, id_conferencier, id_theme, id_salle)"
                    + "VALUES (?,?,?,?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setString(1, c.getTitreConference());
                ps.setDate(2, c.getSqlDate());
                ps.setInt(3, c.getIdConferencier());
                ps.setInt(4, c.getIdTheme());
                ps.setInt(5, c.getIdSalle());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static Conference delet(int _idConference){
            PreparedStatement ps =null;
            String query = "DELET FROM conference WHERE id_conf = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idConference);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(Conference _conf){
            PreparedStatement ps =null;
            String query = "UPDATE conference SET "
                    + "titre_conf = ?,"
                    + "dateCalendar = ?,"
                    + "id_conferencier = ?,"
                    + "id_theme = ?,"
                    + "id_salle = ?"
                    + "WHERE idConference = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);               
                Calendar cal = _conf.getDateConference();
                ps.setString(1, _conf.getTitreConference());
                ps.setDate(2, new java.sql.Date(cal.getTimeInMillis()));
                ps.setInt(3, _conf.getIdConferencier());
                ps.setInt(4, _conf.getIdTheme());
                ps.setInt(5, _conf.getIdSalle());
                ps.setInt(6, _conf.getIdConference());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
