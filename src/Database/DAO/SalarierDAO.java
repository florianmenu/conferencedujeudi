/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.Inscription;
import Database.pojo.Salarier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Florian
 */
public class SalarierDAO {
            public static ArrayList<Salarier> getAll(){
            ArrayList<Salarier> listeDeSalarier = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT * FROM salarier ";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int id_salarier = rs.getInt("id_salarier");
                    String nom_prenom = rs.getString("nom_prenom");
                    Salarier salarier = new Salarier(id_salarier, nom_prenom);
                    listeDeSalarier.add(salarier);
                }
                return listeDeSalarier;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static Salarier get(int id_Salarier){
            PreparedStatement ps =null;
            String query = "SELECT * FROM salarier WHERE id_Salarier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Salarier);
                ResultSet rs = ps.executeQuery();
                String nom_prenom = rs.getString("nom_prenom");
                Salarier salarier = new Salarier(id_Salarier, nom_prenom);
                return salarier;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(Salarier salarier){
            PreparedStatement ps =null;
            String query = "INSERT INTO salarier"
                    + "()"
                    + "VALUES (?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, salarier.getIdSalarie());
                ps.setString(2, salarier.getNomPrenomSalarie());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static Inscription delet(int id_Salarier){
            PreparedStatement ps =null;
            String query = "DELET FROM salarier WHERE id_Salarier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Salarier);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(Salarier salarier){
            PreparedStatement ps =null;
            String query = "UPDATE salarier SET "
                    + "nom_prenom = ?,"
                    + "WHERE id_Salarier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);               
                ps.setString(1, salarier.getNomPrenomSalarie());
                ps.setInt(2, salarier.getIdSalarie());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
