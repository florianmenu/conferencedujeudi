/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.Inscription;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Florian
 */
public class InscriptionDAO {
        public static ArrayList<Inscription> getAll(){
            ArrayList<Inscription> listeDeInscription = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT * FROM inscription";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int idInscription = rs.getInt("id_inscription");
                    int idSalarie = rs.getInt("id_statut");
                    int idConference = rs.getInt("id_conference");
                    Inscription inscription = new Inscription(idInscription, idSalarie, idConference);
                    listeDeInscription.add(inscription);
                }
                return listeDeInscription;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static Inscription get(int _idInscription){
            PreparedStatement ps =null;
            String query = "SELECT * FROM inscription WHERE id_inscription = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idInscription);
                ResultSet rs = ps.executeQuery();
                int idInscription = rs.getInt("id_inscription");
                int idSalarie = rs.getInt("id_salarie");
                int idConference = rs.getInt("idC_cnference");
                Inscription inscription = new Inscription(idInscription, idSalarie, idConference);
                return inscription;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(Inscription inscription){
            PreparedStatement ps =null;
            String query = "INSERT INTO inscription"
                    + "()"
                    + "VALUES (?,?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, inscription.getIdConference());
                ps.setInt(2, inscription.getIdInscription());
                ps.setInt(3, inscription.getIdSalarie());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static Inscription delet(int _idInscription){
            PreparedStatement ps =null;
            String query = "DELET FROM inscription WHERE id_inscription = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idInscription);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(Inscription Inscription){
            PreparedStatement ps =null;
            String query = "UPDATE inscription SET "
                    + "id_conference = ?,"
                    + "id_salarie = ?,"
                    + "WHERE id_inscription = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);               
                ps.setInt(1, Inscription.getIdConference());
                ps.setInt(2, Inscription.getIdSalarie());
                ps.setInt(3, Inscription.getIdInscription());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
