/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.ConferenceStatut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Florian
 */
public class ConferenceStatutDAO {
            //Recupération de tous les conferenciers.
        public static ArrayList<ConferenceStatut> getAll(){
            ArrayList<ConferenceStatut> listeDeStatut = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT * FROM conferenceStatut";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int idConference = rs.getInt("id_conf");
                    int idStatut = rs.getInt("id_statut");

                    ConferenceStatut statut = new ConferenceStatut(idConference, idStatut);
                    listeDeStatut.add(statut);
                }
                return listeDeStatut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'un conferencier par id.
        public static ConferenceStatut get(int _idConferenceStatut){
            PreparedStatement ps =null;
            String query = "SELECT * FROM conferenceStatut WHERE id_conf = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idConferenceStatut);
                ResultSet rs = ps.executeQuery();
                int idConferencier = rs.getInt("id_conf");
                int idStatut = rs.getInt("id_statut");
                ConferenceStatut statut = new ConferenceStatut(idConferencier, idStatut);
                return statut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'un conferencier  
        public static String insert(ConferenceStatut c){
            PreparedStatement ps =null;
            String query = "INSERT INTO conferenceStatut"
                    + "(id_conferencier, idStatut)"
                    + "VALUES (?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, c.getIdConference());
                ps.setInt(2, c.getidStatut());
                ps.execute(); 
                String msg="Nouveau statut crée";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        
        //supression d'un conferencier par id.
        public static void delet(int _idConferencier){
            PreparedStatement ps =null;
            String query = "DELET FROM conferenceStatut WHERE id_conferencier = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, _idConferencier);              
            }catch(SQLException ex){
                System.err.println(ex.getMessage());             
            }
        }
        
         //recherche d'un conferencier par id.
        public static String update(ConferenceStatut _conferencier){
            PreparedStatement ps =null;
            String query = "UPDATE conferenceStatut SET "
                    + "idStatut = ?,"
                    + "WHERE idConference = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);               
                ps.setInt(1, _conferencier.getIdConference());
                ps.setInt(2, _conferencier.getidStatut());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
    
}
