/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.Statut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Florian
 */
public class StatutDAO {
            public static ArrayList<Statut> getAll(){
            ArrayList<Statut> listeDeStatut = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT id_Statut, designation FROM statut";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int id_Statut = rs.getInt("id_Statut");
                    String designation = rs.getString("designation");
                    Statut statut = new Statut(id_Statut, designation);
                    listeDeStatut.add(statut);
                }
                return listeDeStatut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static Statut get(int id_Statut){
            PreparedStatement ps =null;
            String query = "SELECT * FROM statut WHERE id_Statut = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Statut);
                ResultSet rs = ps.executeQuery();
                Statut statut = new Statut(rs.getInt("id_Statut"), rs.getString("designation"));
                return statut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(Statut c){
            PreparedStatement ps =null;
            String query = "INSERT INTO conference"
                    + "(id_Statut, designation)"
                    + "VALUES (?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, c.getIdStatut());
                ps.setString(2, c.getDesignationStatut());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static Statut delet(int id_Statut){
            PreparedStatement ps =null;
            String query = "DELET FROM salle WHERE id_Statut = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Statut);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(Statut statut){
            PreparedStatement ps =null;
            String query = "UPDATE salle SET "
                    + "designation = ?,"
                    + "WHERE id_Statut = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);  
                ps.setString(1,statut.getDesignationStatut());
                ps.setInt(2, statut.getIdStatut());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
