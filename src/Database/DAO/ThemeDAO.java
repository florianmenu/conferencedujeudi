/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.DAO;

import static Database.DatabaseUtilities.getConnexion;
import Database.pojo.Theme;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Florian
 */
public class ThemeDAO {
            public static ArrayList<Theme> getAll(){
            ArrayList<Theme> listeDeStatut = new ArrayList<>();
            PreparedStatement ps =null;
            String query = "SELECT id_Theme, theme FROM statut";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    int id_Statut = rs.getInt("id_Theme");
                    String designation = rs.getString("designation");
                    Theme theme = new Theme(id_Statut, designation);
                    listeDeStatut.add(theme);
                }
                return listeDeStatut;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
        //recherche d'une conference par id.
        public static Theme get(int id_Statut){
            PreparedStatement ps =null;
            String query = "SELECT * FROM theme WHERE id_Theme = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Statut);
                ResultSet rs = ps.executeQuery();
                Theme theme = new Theme(rs.getInt("id_Theme"), rs.getString("designation"));
                return theme;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
           
        //Ajout d'une ligne en base de donné conference   
        public static void insert(Theme c){
            PreparedStatement ps =null;
            String query = "INSERT INTO theme"
                    + "(id_Theme, designation)"
                    + "VALUES (?,?)";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, c.getIdTheme());
                ps.setString(2, c.getDesignationTheme());
                ps.execute();            
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
            }
        }
        
        
        //supression d'une conference par id.
        public static Theme delet(int id_Statut){
            PreparedStatement ps =null;
            String query = "DELET FROM theme WHERE id_Theme = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);
                ps.setInt(1, id_Statut);
                return null;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
        
         //recherche d'une conference par id.
        public static String update(Theme statut){
            PreparedStatement ps =null;
            String query = "UPDATE theme SET "
                    + "designation = ?,"
                    + "WHERE id_Theme = ?";
            try{
                Connection _c = getConnexion();
                ps = _c.prepareStatement(query);   
                ps.setString(1, statut.getDesignationTheme());
                ps.setInt(2,statut.getIdTheme());
                ps.executeQuery();
                String msg = "Ok tout va bien";
                return msg;
            }catch(SQLException ex){
                System.err.println(ex.getMessage());
                return null;
            }
        }
}
