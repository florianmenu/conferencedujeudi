/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Panel;

import Database.DatabaseUtilities;
import Models.ConferenceModel;
import Database.pojo.Conference;
import conferencedujeudi.ConferenceDuJeudi;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Florian
 */
public class Home extends JPanel{
    //declaration du nombre de clic 
    int nbClic = 0;
    
    public Home() {
        
        //Box:
        Box principalBox = Box.createVerticalBox();
        Box labelBox = Box.createVerticalBox();
        Box boutonLaBox = Box.createHorizontalBox();
        Box tableau = Box.createVerticalBox();
        
        //Creation du label dans la box
        JLabel label = new JLabel("Bienvenue sur l'application");
        
        //on ajoute le label dans la box
        labelBox.add(label);
        principalBox.add(labelBox);
        
        //Creation du bouton et de son label
        JButton buton = new JButton("Clic et... Surprise !!!");
        JLabel labelBouton = new JLabel("");
        
        
        //on ajoute le label du bouton dans la box
        boutonLaBox.add(buton);
        
        //On ajoute le bouton dans la box
        boutonLaBox.add(labelBouton);
        principalBox.add(boutonLaBox);
                
        //Action du bouton
        buton.addActionListener(new ActionListener() {
        @Override
            public void actionPerformed(ActionEvent e){
                nbClic += 1;
                labelBouton.setText("Vous avez cliqué " + nbClic + " fois sur le bouton");
            }

        });
         
     
        //Liste ou se trouve les objets Conference
        ArrayList<Conference> listeDeConference = new ArrayList<>();
        try{
            ResultSet rs = DatabaseUtilities.exec("SELECT * from conference");

            while(rs.next()){
                
                String titre = rs.getString("titre_conf");
                int indConf = rs.getInt("id_conf");
                int indConferencier = rs.getInt("id_conferencier");
                Date date2 = rs.getDate("date_conf");
                Calendar cal = Calendar.getInstance();
                cal.setTime(date2);
                int indsalle = rs.getInt("id_salle");
                int indtheme = rs.getInt("id_theme");
                Conference c1 = new Conference(indConf, titre, cal, indConferencier, indsalle, indtheme);
                System.out.println(c1.getNomConferencier());
                listeDeConference.add(c1);
            }
        }catch(SQLException ex){
            Logger.getLogger(ConferenceDuJeudi.class.getName()).log(Level.SEVERE, null, ex);
        }
 
       
        ConferenceModel conferenceModel = new ConferenceModel(listeDeConference);
        
        //Tableau
        JTable table = new JTable(conferenceModel);
        table.setSize(320, 400);
        table.setAutoCreateRowSorter(true);
        
        //on affiche le tableau dans le scrollpane;
        JScrollPane scroll = new JScrollPane(table);
        
        //on ajoute le ytableau dans la box
        tableau.add(scroll);
        principalBox.add(tableau);
        
        //on met toute les box dans la box principal
        this.add(principalBox);
               
    }

}
